package com.example.wordfactory

data class Definitions(
    val definition: String,
    val example : String
)
