package com.example.wordfactory

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import com.example.wordfactory.databinding.FragmentDictionaryBinding
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class FragmentDictionary : Fragment(R.layout.fragment_dictionary) {
    private lateinit var binding: FragmentDictionaryBinding

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding = FragmentDictionaryBinding.bind(view)

        binding.searchButton.setOnClickListener {

            val text = binding.editText.text.toString

            if (text.isNotEmpty()) {
                binding.dictionary1.visibility = View.GONE
                binding.dictionary2.visibility = View.VISIBLE

                NetworkManager.instance.getWordInfo(text)
                    .enqueue(object: Callback<List<WordInfo>>{
                        override fun onResponse(
                            call: Call<List<WordInfo>>,
                            response: Response<List<WordInfo>>
                        ) {
                            if(response.body()!=null)
                                val word = response.body()!![0]
                                binding.cookingTitle.text = word.word
                            binding.pronuncation.text = word.phonetic
                            binding.meaningsText.text = word.meanings
                     
                        }

                        override fun onFailure(call: Call<List<WordInfo>>,
                                               t: Throwable
                        ) {
                           t.printStackTrace()
                        }

                    })
            }
        }
    }
}