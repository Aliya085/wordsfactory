package com.example.wordfactory

data class Meanings(
    val meaningText: String,
    val meaningExample: String
)
