package com.example.wordfactory

object Constants {
    val onboardingPagesList = listOf(
        OnBoardingPage(
            R.drawable.board1,
            "Learn anytime \n" +
                    "and anywhere",
            "Quarantine is the perfect time to spend your day learning something new, from anywhere!",
            R.drawable.tochki1
        ),
        OnBoardingPage(
            R.drawable.board2,
            "Find a course\n" +
                    "for you",
            "Quarantine is the perfect time to spend your day learning something new, from anywhere!",
            R.drawable.tochki2
        ),
        OnBoardingPage(
            R.drawable.board3,
            "Improve your skills",
            "Quarantine is the perfect time to spend your day learning something new, from anywhere!",
            R.drawable.tochki3
        ),

        )


}