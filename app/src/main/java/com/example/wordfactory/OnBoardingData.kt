package com.example.wordfactory

data class OnBoardingPage(
    val image: Int,
    val title: String,
    val subtitle: String,
    val tochki: Int

)
