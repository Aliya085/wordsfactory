package com.example.wordfactory

data class WordInfo(
    val word: String,
    val phonetic: String,
    val meanings: List<Meaning>
)
