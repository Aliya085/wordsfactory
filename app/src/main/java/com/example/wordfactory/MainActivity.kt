package com.example.wordfactory

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.wordfactory.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.bottomNavigation.setOnNavigationItemSelectedListener { item ->
            if (item.itemId == R.id.bottomDictionary)
                supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.fragmentContainer, FragmentDictionary())
                    .commit()

            else if (item.itemId == R.id.bottomVideo)
                supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.fragmentContainer, VideoFragment())
                    .commit()

            true
        }

        supportFragmentManager
            .beginTransaction()
            .replace(R.id.fragmentContainer, FragmentDictionary())
            .commit()
    }
}