package com.example.wordfactory

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import com.example.wordfactory.databinding.FragmentVideoBinding

class VideoFragment : Fragment(R.layout.fragment_video) {

    private lateinit var binding: FragmentVideoBinding

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding = FragmentVideoBinding.bind(view)

        binding.webview.loadUrl("https://google.com")
    }
}