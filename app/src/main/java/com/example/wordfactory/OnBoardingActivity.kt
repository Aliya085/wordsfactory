package com.example.wordfactory

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.viewpager2.widget.ViewPager2
import com.example.wordfactory.databinding.ActivityOnBoardingBinding

class OnBoardingActivity : AppCompatActivity() {

    private lateinit var binding: ActivityOnBoardingBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityOnBoardingBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.onBoardingPager.orientation = ViewPager2.ORIENTATION_HORIZONTAL
        binding.onBoardingPager.adapter = OnBoardingAdapter(this, Constants.onboardingPagesList)

        binding.nextButton.setOnClickListener {
            binding.onBoardingPager.currentItem += 1
        }

        binding.skipButton.setOnClickListener {
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }
    }
}