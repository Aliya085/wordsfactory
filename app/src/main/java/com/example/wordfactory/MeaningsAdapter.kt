package com.example.wordfactory

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.wordfactory.databinding.MeaningsItemBinding

class MeaningsAdapter(
    private val context: Context,
    private val meaningsList: List<Meanings>
) : RecyclerView.Adapter<MeaningsAdapter.ViewHolder>() {

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ViewHolder(LayoutInflater.from(context).inflate(R.layout.meanings_item, parent, false))

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val binding = MeaningsItemBinding.bind(holder.itemView)
    }

    override fun getItemCount() = meaningsList.size
}
