package com.example.wordfactory

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.wordfactory.databinding.OnboardingItemBinding

class OnBoardingAdapter(
    private val context: Context,
    private val pages: List<OnBoardingPage>
) : RecyclerView.Adapter<OnBoardingAdapter.ViewHolder>() {

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(
        LayoutInflater
            .from(context)
            .inflate(R.layout.onboarding_item, parent, false)
    )

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val binding = OnboardingItemBinding.bind(holder.itemView)
        val pages = pages[position]

        binding.onboardingImage.setImageResource(pages.image)
        binding.titleText.text = pages.title
        binding.onboardingText.text = pages.subtitle
        binding.tochki.setImageResource(pages.tochki)
    }

    override fun getItemCount() = pages.size
}
