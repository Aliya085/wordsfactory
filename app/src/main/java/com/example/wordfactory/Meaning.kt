package com.example.wordfactory

data class Meaning(
    val partofspeech: String,
    val definition: List<Definitions>
)
